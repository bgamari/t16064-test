.section .text
.align 8
.align 8
	.quad	4294967301
	.quad	0
	.long	14
	.long	0
Main_X_info:
.Lc251:
	addq $16,%r12
	cmpq 856(%r13),%r12
	ja .Lc255

.Lc254:
	movq $Main_X_con_info,-8(%r12)
	movq %r14,(%r12)
	leaq -7(%r12),%rbx
	jmp *(%rbp)

.Lc255:
	movq $16,904(%r13)
	movl $Main_X_closure,%ebx
	jmp *-8(%r13)

	.size Main_X_info, .-Main_X_info

.section .data
.align 8
.align 1
.globl Main_X_closure
.type Main_X_closure, @object
Main_X_closure:
	.quad	Main_X_info

.section .rodata.str,"aMS",@progbits,1
.align 1
.align 1
.globl Main_zdtczqX3_bytes
.type Main_zdtczqX3_bytes, @object
Main_zdtczqX3_bytes:
	.string "'X"

.section .data
.align 8
.align 1
.globl Main_zdtczqX2_closure
.type Main_zdtczqX2_closure, @object
Main_zdtczqX2_closure:
	.quad	ghczmprim_GHCziTypes_TrNameS_con_info
	.quad	Main_zdtczqX3_bytes

.section .rodata.str,"aMS",@progbits,1
.align 1
.align 1
.globl Main_zdtcX2_bytes
.type Main_zdtcX2_bytes, @object
Main_zdtcX2_bytes:
	.string "X"

.section .data
.align 8
.align 1
.globl Main_zdtcX1_closure
.type Main_zdtcX1_closure, @object
Main_zdtcX1_closure:
	.quad	ghczmprim_GHCziTypes_TrNameS_con_info
	.quad	Main_zdtcX2_bytes

.section .data
.align 8
.align 1
.Lr21Z_closure:
	.quad	ghczmprim_GHCziTypes_KindRepVar_con_info
	.quad	0

.section .data
.align 8
.align 1
.Lr220_closure:
	.quad	ghczmprim_GHCziTypes_ZC_con_info
	.quad	.Lr21Z_closure+2
	.quad	ghczmprim_GHCziTypes_ZMZN_closure+1
	.quad	3

.section .rodata.str,"aMS",@progbits,1
.align 1
.align 1
.globl Main_zdtrModule2_bytes
.type Main_zdtrModule2_bytes, @object
Main_zdtrModule2_bytes:
	.string "Main"

.section .data
.align 8
.align 1
.globl Main_zdtrModule1_closure
.type Main_zdtrModule1_closure, @object
Main_zdtrModule1_closure:
	.quad	ghczmprim_GHCziTypes_TrNameS_con_info
	.quad	Main_zdtrModule2_bytes

.section .rodata.str,"aMS",@progbits,1
.align 1
.align 1
.globl Main_zdtrModule4_bytes
.type Main_zdtrModule4_bytes, @object
Main_zdtrModule4_bytes:
	.string "main"

.section .data
.align 8
.align 1
.globl Main_zdtrModule3_closure
.type Main_zdtrModule3_closure, @object
Main_zdtrModule3_closure:
	.quad	ghczmprim_GHCziTypes_TrNameS_con_info
	.quad	Main_zdtrModule4_bytes

.section .data
.align 8
.align 1
.globl Main_zdtrModule_closure
.type Main_zdtrModule_closure, @object
Main_zdtrModule_closure:
	.quad	ghczmprim_GHCziTypes_Module_con_info
	.quad	Main_zdtrModule3_closure+1
	.quad	Main_zdtrModule1_closure+1
	.quad	3

.section .data
.align 8
.align 1
.globl Main_zdtcX_closure
.type Main_zdtcX_closure, @object
Main_zdtcX_closure:
	.quad	ghczmprim_GHCziTypes_TyCon_con_info
	.quad	Main_zdtrModule_closure+1
	.quad	Main_zdtcX1_closure+1
	.quad	ghczmprim_GHCziTypes_krepzdztArrzt_closure+4
	.quad	6136962148358085538
	.quad	2047526523769221729
	.quad	0
	.quad	3

.section .data
.align 8
.align 1
.Lr221_closure:
	.quad	ghczmprim_GHCziTypes_KindRepTyConApp_con_info
	.quad	Main_zdtcX_closure+1
	.quad	.Lr220_closure+2
	.quad	3

.section .data
.align 8
.align 1
.globl Main_zdtczqX1_closure
.type Main_zdtczqX1_closure, @object
Main_zdtczqX1_closure:
	.quad	ghczmprim_GHCziTypes_KindRepFun_con_info
	.quad	.Lr21Z_closure+2
	.quad	.Lr221_closure+1
	.quad	3

.section .data
.align 8
.align 1
.globl Main_zdtczqX_closure
.type Main_zdtczqX_closure, @object
Main_zdtczqX_closure:
	.quad	ghczmprim_GHCziTypes_TyCon_con_info
	.quad	Main_zdtrModule_closure+1
	.quad	Main_zdtczqX2_closure+1
	.quad	Main_zdtczqX1_closure+4
	.quad	5225325308912582631
	.quad	-3463441427315994638
	.quad	1
	.quad	3

.section .text
.align 8
.align 8
	.quad	4294967300
	.quad	0
	.long	14
	.long	0
.globl Main_zdwgo_info
.type Main_zdwgo_info, @function
Main_zdwgo_info:
.Lc25x:
	jmp .Lc25p
.Lc25w:
	addq $-16,%r12
	decq %r14
.Lc25p:
	addq $16,%r12
	cmpq 856(%r13),%r12
	ja .Lc25B
.Lc25A:
	testq %r14,%r14
	jg .Lc25w
.Lc25v:
	movq $ghczmprim_GHCziTypes_Izh_con_info,-8(%r12)
	movq %r14,(%r12)
	leaq -7(%r12),%rbx
	jmp *(%rbp)
.Lc25B:
	movq $16,904(%r13)
	movl $Main_zdwgo_closure,%ebx
	jmp *-8(%r13)
	.size Main_zdwgo_info, .-Main_zdwgo_info

.section .data
.align 8
.align 1
.globl Main_zdwgo_closure
.type Main_zdwgo_closure, @object
Main_zdwgo_closure:
	.quad	Main_zdwgo_info

.section .text
.align 8
.align 8
	.quad	4294967301
	.quad	0
	.long	14
	.long	0
.globl Main_slowzugo_info
.type Main_slowzugo_info, @function
Main_slowzugo_info:
.Lc25Q:
	leaq -8(%rbp),%rax
	cmpq %r15,%rax
	jb .Lc25Y
.Lc25Z:
	movq $.Lc25N_info,-8(%rbp)
	movq %r14,%rbx
	addq $-8,%rbp
	testb $7,%bl
	jne .Lc25N
.Lc25O:
	jmp *(%rbx)
.align 8
	.quad	0
	.long	30
	.long	0
.Lc25N_info:
.Lc25N:
	movq $.Lc25T_info,(%rbp)
	movq 7(%rbx),%r14
	jmp Main_zdwgo_info
.Lc25Y:
	movl $Main_slowzugo_closure,%ebx
	jmp *-8(%r13)
.align 8
	.quad	0
	.long	30
	.long	0
.Lc25T_info:
.Lc25T:
	addq $16,%r12
	cmpq 856(%r13),%r12
	ja .Lc263
.Lc262:
	movq $Main_X_con_info,-8(%r12)
	movq %rbx,(%r12)
	leaq -7(%r12),%rbx
	addq $8,%rbp
	jmp *(%rbp)
.Lc263:
	movq $16,904(%r13)
	jmp stg_gc_unpt_r1
	.size Main_slowzugo_info, .-Main_slowzugo_info

.section .data
.align 8
.align 1
.globl Main_slowzugo_closure
.type Main_slowzugo_closure, @object
Main_slowzugo_closure:
	.quad	Main_slowzugo_info

.section .text
.align 8
.align 8
	.quad	4294967301
	.quad	0
	.long	14
	.long	0
.globl Main_slow_info
.type Main_slow_info, @function
Main_slow_info:
.Lc26f:
	leaq -8(%rbp),%rax
	cmpq %r15,%rax
	jb .Lc26g
.Lc26h:
	movq $.Lc26c_info,-8(%rbp)
	movq %r14,%rbx
	addq $-8,%rbp
	testb $7,%bl
	jne .Lc26c
.Lc26d:
	jmp *(%rbx)
.align 8
	.quad	0
	.long	30
	.long	0
.Lc26c_info:
.Lc26c:
	movq 7(%rbx),%r14
	addq $8,%rbp
	jmp Main_zdwgo_info
.Lc26g:
	movl $Main_slow_closure,%ebx
	jmp *-8(%r13)
	.size Main_slow_info, .-Main_slow_info

.section .data
.align 8
.align 1
.globl Main_slow_closure
.type Main_slow_closure, @object
Main_slow_closure:
	.quad	Main_slow_info

.section .text
.align 8
.align 8
	.quad	0
	.long	21
	.long	0
.globl Main_main2_info
.type Main_main2_info, @function
Main_main2_info:
.Lc26x:
	leaq -24(%rbp),%rax
	cmpq %r15,%rax
	jb .Lc26D
.Lc26E:
	subq $8,%rsp
	movq %r13,%rax
	movq %rbx,%rsi
	movq %rax,%rdi
	xorl %eax,%eax
	call newCAF
	addq $8,%rsp
	testq %rax,%rax
	je .Lc26u
.Lc26t:
	movq $stg_bh_upd_frame_info,-16(%rbp)
	movq %rax,-8(%rbp)
	movq $.Lc26v_info,-24(%rbp)
	movq $10000000000,%r14
	addq $-24,%rbp
	jmp Main_zdwgo_info
.Lc26u:
	jmp *(%rbx)
.Lc26D:
	jmp *-16(%r13)
.align 8
	.quad	0
	.long	30
	.long	0
.Lc26v_info:
.Lc26v:
	movq $.Lc26A_info,(%rbp)
	movl $ghczmprim_GHCziTypes_ZMZN_closure+1,%edi
	movq 7(%rbx),%rsi
	xorl %r14d,%r14d
	jmp base_GHCziShow_zdwshowSignedInt_info
.align 8
	.quad	0
	.long	30
	.long	0
.Lc26A_info:
.Lc26A:
	addq $24,%r12
	cmpq 856(%r13),%r12
	ja .Lc26I
.Lc26H:
	movq $ghczmprim_GHCziTypes_ZC_con_info,-16(%r12)
	movq %rbx,-8(%r12)
	movq %r14,(%r12)
	leaq -14(%r12),%rbx
	addq $8,%rbp
	jmp *(%rbp)
.Lc26I:
	movq $24,904(%r13)
	jmp stg_gc_pp
	.size Main_main2_info, .-Main_main2_info

.section .data
.align 8
.align 1
.globl Main_main2_closure
.type Main_main2_closure, @object
Main_main2_closure:
	.quad	Main_main2_info
	.quad	0
	.quad	0
	.quad	0

.section .text
.align 8
.align 8
	.quad	4294967299
	.quad	3
	.long	14
	.long	0
.globl Main_main1_info
.type Main_main1_info, @function
Main_main1_info:
.Lc26S:
	movl $ghczmprim_GHCziTypes_True_closure+2,%edi
	movl $Main_main2_closure,%esi
	movl $base_GHCziIOziHandleziFD_stdout_closure,%r14d
	jmp base_GHCziIOziHandleziText_hPutStr2_info
	.size Main_main1_info, .-Main_main1_info

.section .data
.align 8
.align 1
.globl Main_main1_closure
.type Main_main1_closure, @object
Main_main1_closure:
	.quad	Main_main1_info
	.quad	base_GHCziIOziHandleziText_hPutStr2_closure
	.quad	base_GHCziIOziHandleziFD_stdout_closure
	.quad	Main_main2_closure
	.quad	0

.section .text
.align 8
.align 8
	.quad	4294967299
	.quad	0
	.long	14
	.long	Main_main1_closure-(Main_main_info)+0
.globl Main_main_info
.type Main_main_info, @function
Main_main_info:
.Lc272:
	jmp Main_main1_info
	.size Main_main_info, .-Main_main_info

.section .data
.align 8
.align 1
.globl Main_main_closure
.type Main_main_closure, @object
Main_main_closure:
	.quad	Main_main_info
	.quad	0

.section .text
.align 8
.align 8
	.quad	4294967299
	.quad	2
	.long	14
	.long	0
.globl Main_main3_info
.type Main_main3_info, @function
Main_main3_info:
.Lc27c:
	movl $Main_main1_closure+1,%r14d
	jmp base_GHCziTopHandler_runMainIO1_info
	.size Main_main3_info, .-Main_main3_info

.section .data
.align 8
.align 1
.globl Main_main3_closure
.type Main_main3_closure, @object
Main_main3_closure:
	.quad	Main_main3_info
	.quad	Main_main1_closure
	.quad	base_GHCziTopHandler_runMainIO1_closure
	.quad	0

.section .text
.align 8
.align 8
	.quad	4294967299
	.quad	0
	.long	14
	.long	Main_main3_closure-(ZCMain_main_info)+0
.globl ZCMain_main_info
.type ZCMain_main_info, @function
ZCMain_main_info:
.Lc27m:
	jmp Main_main3_info
	.size ZCMain_main_info, .-ZCMain_main_info

.section .data
.align 8
.align 1
.globl ZCMain_main_closure
.type ZCMain_main_closure, @object
ZCMain_main_closure:
	.quad	ZCMain_main_info
	.quad	0

.section .text
.align 8
.align 8
	.quad	4294967301
	.quad	0
	.long	14
	.long	0
.globl Main_runX_info
.type Main_runX_info, @function
Main_runX_info:
.Lc27z:
	leaq -8(%rbp),%rax
	cmpq %r15,%rax
	jb .Lc27A
.Lc27B:
	movq $.Lc27w_info,-8(%rbp)
	movq %r14,%rbx
	addq $-8,%rbp
	testb $7,%bl
	jne .Lc27w
.Lc27x:
	jmp *(%rbx)
.align 8
	.quad	0
	.long	30
	.long	0
.Lc27w_info:
.Lc27w:
	movq 7(%rbx),%rbx
	addq $8,%rbp
	jmp stg_ap_0_fast
.Lc27A:
	movl $Main_runX_closure,%ebx
	jmp *-8(%r13)
	.size Main_runX_info, .-Main_runX_info

.section .data
.align 8
.align 1
.globl Main_runX_closure
.type Main_runX_closure, @object
Main_runX_closure:
	.quad	Main_runX_info

.section .text
.align 8
.align 8
	.quad	4294967301
	.quad	0
	.long	14
	.long	0
.globl Main_zdWX_info
.type Main_zdWX_info, @function
Main_zdWX_info:
.Lc27Q:
	leaq -8(%rbp),%rax
	cmpq %r15,%rax
	jb .Lc27R
.Lc27S:
	movq $.Lc27N_info,-8(%rbp)
	movq %r14,%rbx
	addq $-8,%rbp
	jmp stg_ap_0_fast
.Lc27R:
	movl $Main_zdWX_closure,%ebx
	jmp *-8(%r13)
.align 8
	.quad	0
	.long	30
	.long	0
.Lc27N_info:
.Lc27N:
	addq $16,%r12
	cmpq 856(%r13),%r12
	ja .Lc27V
.Lc27U:
	movq $Main_X_con_info,-8(%r12)
	movq %rbx,(%r12)
	leaq -7(%r12),%rbx
	addq $8,%rbp
	jmp *(%rbp)
.Lc27V:
	movq $16,904(%r13)
	jmp stg_gc_unpt_r1
	.size Main_zdWX_info, .-Main_zdWX_info

.section .data
.align 8
.align 1
.globl Main_zdWX_closure
.type Main_zdWX_closure, @object
Main_zdWX_closure:
	.quad	Main_zdWX_info

.section .rodata.str,"aMS",@progbits,1
.align 1
.align 1
i281_str:
	.string "main:Main.X"

.section .text
.align 8
.align 8
	.long	i281_str-(Main_X_con_info)+0
	.long	0
	.quad	1
	.long	2
	.long	0
.globl Main_X_con_info
.type Main_X_con_info, @object
Main_X_con_info:
.Lc280:
	incq %rbx
	jmp *(%rbp)
	.size Main_X_con_info, .-Main_X_con_info

.section .note.GNU-stack,"",@progbits
.ident "GHC 9.3.20210505"


